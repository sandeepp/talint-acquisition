var express = require("express");
var app = express();
var bodyParser = require('body-parser');
var path = require("path");
var fs = require('fs');
var pdfcrowd = require("pdfcrowd");
var webshot = require('node-webshot');
var gm = require('gm').subClass({ imageMagick: true });
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//apikey b28cc402d6a5547b8fa0b6d7faaf3bee username penjerlasandeep
app.get('/', function (req, res) {

    var templateString = '<html lang="en"><head><title>Bootstrap Example</title><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script><script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script></head><body><div class="container"><h1 class="container">h1 Bootstrap heading (36px)</h1><h2>h2 Bootstrap heading (30px)</h2><h3>h3 Bootstrap heading (24px)</h3><h4>h4 Bootstrap heading (18px)</h4><h5>h5 Bootstrap heading (14px)</h5><h6>h6 Bootstrap heading (12px)</h6></div></body></html>'
    var drinks = [
        { name: 'Bloody Mary', drunkness: 3 },
        { name: 'Martini', drunkness: 5 },
        { name: 'Scotch', drunkness: 10 }
    ];
    var tagline = "Any code of your own that you haven't looked at for six or more months might as well have been written by someone else.";

    var client = new pdfcrowd.HtmlToImageClient("penjerlasandeep", "b28cc402d6a5547b8fa0b6d7faaf3bee");

    // configure the conversion
    try {
        client.setOutputFormat("png");
    } catch (why) {
        console.error("Pdfcrowd Error: " + why);
        console.error("Pdfcrowd Error Code: " + why.getCode());
        console.error("Pdfcrowd Error Message: " + why.getMessage());
        process.exit(1);
    }

    // run the conversion and write the result to a file
    client.convertStringToFile(
        templateString, "./public/images/sample.png",
        function (err, fileName) {
            if (err) {
                res.json({ status: 'failure', msg: err })
                return console.error("Pdfcrowd Error: " + err);

            }
            console.log("Success: the file was created " + fileName);
            res.json({ status: 'success', 'imgUrl': '' })
        });
    res.render(__dirname + '/public/index', {
        drinks: drinks,
        tagline: tagline
    });
    //res.sendFile(path.join(__dirname+'/public/index.html'));

});

app.post('/getImage', function (req, res) {
    var request = req.body;
    var protocol = req.protocol;
    var hostname = req.headers.host;
    // var request = {
    //   "list":[{"name":"sandeep","age":27},{"name":"dinakr","age":25},{"name":"kalyan","age":25}]
    // }
    //var templateString='<html lang="en"><head><title>Bootstrap Example</title><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script><script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script></head><body><div class="container"><h1 class="container">h1 Bootstrap heading (36px)</h1><h2>h2 Bootstrap heading (30px)</h2><h3>h3 Bootstrap heading (24px)</h3><h4>h4 Bootstrap heading (18px)</h4><h5>h5 Bootstrap heading (14px)</h5><h6>h6 Bootstrap heading (12px)</h6></div></body></html>'
    var templateString = '<html lang="en"> <head> <title>Bootstrap Example</title> <meta charset="utf-8"> <meta name="viewport" content="width=device-width, initial-scale=1"> <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script><script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> <style>body{font-family: "Montserrat", sans-serif;}.list-group li{list-style: none;}.panel-info, .panel-rating, .panel-more1{float: left;margin: 0 10px;}.table>tbody>tr>td{border-top: none;vertical-align: middle;font-size: 20px;font-weight: 400;}.table>thead{background-color:#1e94ae;font-weight: 500;color: white;font-size: 16px;}.progress{margin-bottom: 0px;border-radius: 0px;background-color: #d4d4d4;}.row-eq-height{display: flex;align-items: center;}.borderStyle{border:1px solid #ddd;}.custom-margin{margin-bottom: 20px;}.fontSize{font-size: 40px;font-weight: 600;}.green{background-color:#6a9258;}.blue{background-color:#2e6eac;}.yellow{background-color: #ffa73a;}.star-ratings-sprite{background: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/2605/star-rating-sprite.png") repeat-x; font-size: 0; height: 21px; line-height: 0; overflow: hidden; text-indent: -999em; width: 110px; margin: 0 auto;}.star-ratings-sprite-rating{background: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/2605/star-rating-sprite.png") repeat-x; background-position: 0 100%; float: left; height: 21px; display: block;}</style> </head> <body> <div class="container"> <div class="row row-eq-height" style="height: 200px;margin-top: 30px;padding: 10px;box-shadow: 0px 0px 2px 2px #ddd;"> <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3"> <div class="row"> <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4 "> <img src="' + request.profile_image + '" class="img-circle img-responsive" style="width:100px;"/> </div><div class="col-lg-8 col-sm-8 col-md-8 col-xs-8"> <h4 style="word-wrap: break-word;">' + request.name + '</h4> <span>' + request.designation + '</span> <br><span style="font-size:16px;color:#858585;">' + request.location + '</span> </div></div></div>'
    if (request.match != undefined) { 
        templateString += '<div class="col-lg-1 col-sm-1 col-md-1 col-sm-1" style="padding-top:35px;"> <div class=""> <span> Match </span> <label style="color: #00b2d6;font-weight: bold;font-size:30px;"> ' + request.match + ' </label> </div></div>'
    }
    templateString += '<div class="col-lg-3 col-sm-3 col-md-3 col-xs-3"> <div class="row" style="padding:10px;border-bottom:1px solid #ddd;">'
     if(request.experiance.relevantexperience != undefined){
      templateString += '<div class="col-lg-6 col-md-6 col-xs-6 col-sm-6" style="word-wrap: break-word;font-size:12px;"> Relevant Experience </div><div class="col-lg-6 col-md-6 col-xs-6 col-sm-6"> <span> <span style="font-size:15px;"> <b>' + request.experiance.relevantexperience + '</b> </span></span> </div></div><div class="row" style="padding:10px;">' 
     }
     if(request.experiance.totalexperience != undefined){
     templateString += '<div class="col-lg-6 col-md-6 col-xs-6 col-sm-6" style="word-wrap: break-word;font-size:12px;"> Total Experience </div><div class="col-lg-6 col-md-6 col-xs-6 col-sm-6"><span style="font-size:15px;"><b>' + request.experiance.totalexperience + '</b> </span></span> </div></div></div>'
     } 
     templateString +='<div class="col-lg-3 col-sm-3 col-md-3 "> <div class="row" style="padding:5px;">'
     for(var i=0;i<request.skills.length;i++ ){ 
        if(request.skills[i].name != undefined  || request.skills[i].value != undefined){
        templateString += '<div class="col-lg-6 col-md-6 col-xs-6 col-sm-6"> ' + request.skills[i].name + ' </div> <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6"><div class="star-ratings-sprite"> <span style="width:' + request.skills[i].value  + '%" class="star-ratings-sprite-rating"> <span class="dot"></span> </span> </div></div>'
     } 
    }
    // if (request.skillname1 != undefined) {
    //     templateString += '<div class="col-lg-6 col-md-6 col-xs-6 col-sm-6"> ' + request.skillname1 + ' </div>'
    // }
    // if (request.skillvalue1 != undefined) {
    //     templateString += '<div class="col-lg-6 col-md-6 col-xs-6 col-sm-6"><div class="star-ratings-sprite"> <span style="width:' + request.skillvalue1 + '%" class="star-ratings-sprite-rating"> <span class="dot"></span> </span> </div></div>'
    // }
    // if (request.skillname2 != undefined) {
    //     templateString += '</div><div class="row" style="padding:5px;"> <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6"> ' + request.skillname2 + ' </div>'
    // } if (request.skillvalue2 != undefined) {
    //     templateString += '<div class="col-lg-6 col-md-6 col-xs-6 col-sm-6"> <div class="star-ratings-sprite"> <span style="width:' + request.skillvalue2 + '%" class="star-ratings-sprite-rating"></span> </div></div>'
    // }
    // if (request.skillname3 != undefined) {
    //     templateString += '</div><div class="row" style="padding:5px;"> <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6"> ' + request.skillname3 + ' </div>'
    // }
    // if (request.skillvalue3 != undefined) {
    //     templateString += '<div class="col-lg-6 col-md-6 col-xs-6 col-sm-6"> <div class="star-ratings-sprite"> <span style="width:' + request.skillvalue3 + '%" class="star-ratings-sprite-rating"></span> </div></div>'
    // }
    // if (request.skillname4 != undefined) {
    //     templateString += '</div><div class="row" style="padding:5px;"> <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6"> ' + request.skillname4 + ' </div>'
    // } if (request.skillvalue4 != undefined) {
    //     templateString += '<div class="col-lg-6 col-md-6 col-xs-6 col-sm-6"> <div class="star-ratings-sprite"> <span style="width:' + request.skillvalue4 + '%" class="star-ratings-sprite-rating"></span> </div></div>'
    // }
    // if (request.skillname5 != undefined) {
    //     templateString += '</div><div class="row" style="padding:5px;" > <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6"> ' + request.skillname5 + ' </div>'
    // } if (request.skillvalue5 != undefined) {
    //     templateString += '<div class="col-lg-6 col-md-6 col-xs-6 col-sm-6"> <div class="star-ratings-sprite"> <span style="width:' + request.skillvalue5 + '%" class="star-ratings-sprite-rating"></span> </div></div>'
    // }
    templateString += '</div></div>'
    if (request.joiningProbability != undefined) {
        templateString += '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center"> <span>Joining probability</span> <h2><b>' + request.joiningProbability + '</b></h2> </div>'
    }
    templateString += '</div></div></body></html>'
    var d = new Date();
    var timeStamp = d.getTime();
    var name = 'image_' + timeStamp;
    webshot(templateString, './public/images/' + name + '.png', { siteType: 'html' }, function (err) {
        console.log("hello");
        var d1 = new Date();
        var timeStamp1 = d1.getTime();
        var name1 = 'image_' + timeStamp1;
        gm('./public/images/' + name + '.png')
            .crop(1500, 270)
            .write('./public/images/' + name1 + '.png', (err) => {
                if (err) {
                    console.log(err);
                } else {
                    fs.unlink('./public/images/' + name + '.png', (err) => {
                        if (err) throw err;
                        res.json({ "status": "success", "img_url": protocol + "://" + hostname + "/images/" + name1 + ".png" })
                    });

                }
            })
    });

    //res.sendFile(__dirname+'/public/images/'+name+'.png');


});


app.all('/*', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

app.set('port', (process.env.PORT || 2000));
app.listen(app.get('port'), function () {
    console.log('Node app is running on port', app.get('port'));


});
console.log("Running at Port 2000");
